package com.zhuawa.course.biz.user;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zhuawa.course.biz.user.customerplugin.WebLog;
import com.zhuawa.course.biz.user.service.UserService;
import com.zhuawa.course.persistence.user.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * @author apple on 2019/12/7.
 * @version 1.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {
    @Resource
    private UserService userService;

    @Test
    @WebLog
    public void testDS() {
        //当前页
        int currentPage = 1 ;
        //每页大小
        int pageSize = 20 ;
        Page page = new Page(currentPage,pageSize);
        IPage<User> userIPage = userService.UserIpageDS(page);
        System.out.println(userIPage.getSize());

    }
}
